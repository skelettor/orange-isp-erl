#!/bin/sh -e

DESTFILE=/sbin/dhclient3.old

if [ -f "$DESTFILE" ]
then
        echo "Patched dhclient already there"
        exit 0
fi

mv /sbin/dhclient3 /sbin/dhclient3.old
cp -f /config/packages/dhclient3 /sbin/dhclient3

chmod 755 /sbin/dhclient3

