#!/bin/sh -e

DESTFILE=/usr/sbin/dibbler-client

if [ -f "$DESTFILE" ]
then
        echo "Dibbler-client already there"
        exit 0
fi

export DEBIAN_FRONTEND=noninteractive
debconf-set-selections < /config/packages/dibbler_preseed.txt
dpkg -i /config/packages/dibbler.deb

cp -f /config/packages/client.conf /etc/dibbler
cp /config/packages/radvd.sh /etc/dibbler
chmod 755 /etc/dibbler/radvd.sh

service dibbler-client restart