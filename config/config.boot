firewall {
    all-ping enable
    broadcast-ping disable
    ipv6-name WANv6_IN {
        default-action drop
        description "WANv6 inbound traffic forwarded to LAN"
        rule 10 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
        rule 30 {
            action accept
            description "Allow ICMPv6"
            log disable
            protocol icmpv6
        }
    }
    ipv6-name WANv6_LOCAL {
        default-action drop
        description "WANv6 inbound traffic to the router"
        rule 10 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
        rule 30 {
            action accept
            description "Allow ICMPv6"
            log disable
            protocol icmpv6
        }
        rule 40 {
            action accept
            description "Allow DHCPv6"
            destination {
                port 546
            }
            protocol udp
            source {
                port 547
            }
        }
    }
    ipv6-receive-redirects disable
    ipv6-src-route disable
    ip-src-route disable
    log-martians enable
    name WAN_IN {
        default-action drop
        description "WAN to internal"
        rule 10 {
            action accept
            description "Allow established/related"
            log disable
            protocol all
            state {
                established enable
                invalid disable
                new disable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            log disable
            protocol all
            state {
                established disable
                invalid enable
                new disable
                related disable
            }
        }
    }
    name WAN_LOCAL {
        default-action drop
        description "WAN to router"
        rule 1 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 3 {
            action drop
            description "Drop invalid state"
            log disable
            state {
                invalid enable
            }
        }
    }
    receive-redirects disable
    send-redirects enable
    source-validation disable
    syn-cookies enable
}
interfaces {
    ethernet eth0 {
        address 192.168.100.254/24
        description LAN_ETH0
        duplex auto
        ipv6 {
            dup-addr-detect-transmits 1
            router-advert {
                cur-hop-limit 64
                link-mtu 0
                managed-flag false
                max-interval 600
                other-config-flag false
                prefix ::/64 {
                    autonomous-flag true
                    on-link-flag true
                    preferred-lifetime 14400
                    valid-lifetime 18000
                }
                reachable-time 0
                retrans-timer 0
                send-advert true
            }
        }
        speed auto
    }
    ethernet eth1 {
        description ISP
        duplex auto
        mtu 1500
        speed auto
        vif 832 {
            address dhcp
            description ISP_DATA
            dhcp-options {
                client-option "send vendor-class-identifier &quot;sagem&quot;;"
                client-option "request subnet-mask, routers, domain-name-servers, domain-name, broadcast-address, dhcp-lease-time, dhcp-renewal-time, dhcp-rebinding-time, rfc3118-auth;"
                client-option "send rfc3118-auth 00:00:00:00:00:00:00:00:00:00:00:1a:09:00:00:05:58:01:03:41:01:0d:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:3c:12:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:03:13:zz:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh;"
                client-option "send user-class &quot;\053FSVDSL_livebox.Internet.softathome.Livebox4&quot;;"
                client-option "send dhcp-client-identifier 1:YY:YY:YY:YY:YY:YY;"
                default-route update
                default-route-distance 210
                global-option "option rfc3118-auth code 90 = string;"
                name-server update
            }
            egress-qos "0:0 1:0 2:0 3:0 4:0 5:0 6:6 7:0"
            firewall {
                in {
                    ipv6-name WANv6_IN
                    name WAN_IN
                }
                local {
                    ipv6-name WANv6_LOCAL
                    name WAN_LOCAL
                }
            }
            ipv6 {
                address {
                    autoconf
                }
                dup-addr-detect-transmits 1
            }
            mtu 1500
        }
        vif 840 {
            address 192.168.255.254/32
            description ISP_TV_STREAM
            egress-qos "0:5 1:5 2:5 3:5 4:5 5:5 6:5 7:5"
        }
    }
    ethernet eth2 {
        description LIVEBOX
        duplex auto
        speed auto
        vif 832 {
            address 192.168.254.254/24
            description Voip
        }
    }
    loopback lo {
    }
}
port-forward {
    auto-firewall enable
    hairpin-nat enable
    lan-interface eth0
    wan-interface eth1.832
}
protocols {
    igmp-proxy {
        disable-quickleave
        interface eth0 {
            alt-subnet 0.0.0.0/0
            role downstream
            threshold 1
        }
        interface eth1 {
            role disabled
            threshold 1
        }
        interface eth1.832 {
            role disabled
            threshold 1
        }
        interface eth1.840 {
            alt-subnet 0.0.0.0/0
            role upstream
            threshold 1
        }
        interface eth2 {
            role disabled
            threshold 1
        }
        interface eth2.832 {
            role disabled
            threshold 1
        }
    }
}
service {
    dhcp-server {
        disabled false
        global-parameters "option rfc3118-auth code 90 = string;"
        global-parameters "option SIP code 120 = string;"
        global-parameters "option Vendor-specific code 125 = string;"
        hostfile-update disable
        shared-network-name LAN_ETH0_DHCP {
            authoritative enable
            subnet 192.168.100.0/24 {
                default-router 192.168.100.254
                dns-server 192.168.100.254
                lease 86400
                ntp-server 192.168.100.254
                start 192.168.100.100 {
                    stop 192.168.100.200
                }
                static-mapping LIVEBOX-TV {
                    ip-address 192.168.100.10
                    mac-address ZZ:ZZ:ZZ:ZZ:ZZ:ZZ
                    static-mapping-parameters "option domain-name-servers 80.10.246.3,81.253.149.10;"
                    static-mapping-parameters "option Vendor-specific 00:00:0d:e9:24:04:06:YY:YY:YY:YY:YY:YY:05:0f:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:06:09:4c:69:76:65:62:6f:78:20:VV;"
                }
            }
        }
        shared-network-name LAN_ETH2_LIVEBOX {
            authoritative enable
            subnet 192.168.254.0/24 {
                default-router 192.168.254.254
                dns-server 80.10.246.136
                dns-server 81.253.149.6
                lease 86400
                start 192.168.254.21 {
                    stop 192.168.254.200
                }
                static-mapping Livebox {
                    ip-address 192.168.254.1
                    mac-address YY:YY:YY:YY:YY:YY
                }
                subnet-parameters "option SIP 0:6:73:62:63:74:33:67:3:50:55:54:6:61:63:63:65:73:73:11:6f:72:61:6e:67:65:2d:6d:75:6c:74:69:6d:65:64:69:61:3:6e:65:74:0;"
                subnet-parameters "option Vendor-specific 00:00:05:58:0c:01:0a:00:00:00:00:00:ff:ff:ff:ff:ff;"
                subnet-parameters "option rfc3118-auth 00:00:00:00:00:00:00:00:00:00:00:64:68:63:70:6c:69:76:65:62:6f:78:66:72:32:35:30;"
                subnet-parameters "option domain-search &quot;XXX.access.orange-multimedia.net.&quot;;"
                subnet-parameters "option domain-name &quot;orange.fr&quot;;"
            }
        }
        static-arp disable
        use-dnsmasq disable
    }
    dns {
        forwarding {
            cache-size 1024
            listen-on lo
            listen-on eth0
            listen-on eth2
            listen-on eth0.50
            name-server 1.1.1.1
            name-server 9.9.9.9
            name-server 2606:4700:4700::1111
            name-server 2620:fe::fe
        }
    }
    gui {
        http-port 80
        https-port 443
        listen-address 192.168.100.254
        older-ciphers disable
    }
    nat {
        rule 5001 {
            description "MASQ: WAN"
            log disable
            outbound-interface eth1.832
            protocol all
            type masquerade
        }
    }
    ssh {
        listen-address 192.168.100.254
        port 22
        protocol-version v2
    }
    ubnt-discover {
        disable
    }
    unms {
        disable
    }
    upnp2 {
        listen-on eth0
        nat-pmp enable
        port 34651
        secure-mode enable
        wan eth1.832
    }
}
system {
    config-management {
        commit-revisions 5
    }
    conntrack {
        expect-table-size 4096
        hash-size 4096
        table-size 32768
        tcp {
            half-open-connections 512
            loose disable
            max-retrans 3
        }
    }
    host-name erl
    ip {
        override-hostname-ip 192.168.100.254
    }
    name-server 127.0.0.1
    ntp {
        server 0.ubnt.pool.ntp.org {
        }
        server 1.ubnt.pool.ntp.org {
        }
        server 2.ubnt.pool.ntp.org {
        }
        server 3.ubnt.pool.ntp.org {
        }
    }
    offload {
        hwnat disable
        ipsec enable
        ipv4 {
            forwarding enable
            gre enable
            vlan enable
        }
        ipv6 {
            forwarding enable
            vlan enable
        }
    }
    syslog {
        global {
            facility all {
                level notice
            }
            facility protocols {
                level warning
            }
        }
    }
    task-scheduler {
        task daily_restart_igmp-proxy {
            crontab-spec "0 3 * * *"
            executable {
                path /config/scripts/igmp-proxy_restart.sh
            }
        }
    }
    time-zone Europe/Paris
    traffic-analysis {
        dpi disable
        export enable
    }
}
