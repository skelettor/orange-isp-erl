erl
=========

Description
------------

ERL Router Configuration for Orange ISP Provider

Work with ERL Firmware >= 1.10

SK-ERL Configuration for Orange Fiber Provider
 - Orange DHCP Authentication OK
 - IPv4 and IPv6 Configuration OK
 - TV Stream, Replay and VOD on OK

Cabling Specifications
 - ONT Device connected to eth1
 - LAN Switch on eth0
 - eth2 reserved for Livebox for VOIP manangement

LAN DHCP Server Specification : 
 - Subnet 192.168.100.0/24
 - Gateway 192.168.100.254
 - DHCP Range 192.168.100.100 - 192.168.100.200
 - DNS Server : 
    -  CloudFare : 1.1.1.1
    -  Quad9 : 9.9.9.9
-  Static Mapping for : 
    -  LIVEBOX-TV (192.168.100.10) : Orange DNS + Option 125

To edit before using
------------
In config/config.boot : 
 - Line 159 : DHCP Option 90 Orange Authentication String : 
       
    More information source : 
    - https://lafibre.info/remplacer-livebox/cacking-nouveau-systeme-de-generation-de-loption-90-dhcp/
        
            client-option "send rfc3118-auth 00:00:00:00:00:00:00:00:00:00:00:1a:09:00:00:05:58:01:03:41:01:0d:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:3c:12:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:03:13:zz:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh;"

    Replace `XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX` by your Orange ISP Identifier "fti/xxxxxx" encoded in Hexa.

    Using Livebox Into tool (https://www.liveboxinfo.ga/) to retrieve salt and hash value for replacing value :  `3c:12:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:03:13:zz:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh` ( https://lafibre.info/remplacer-livebox/renew-dhcp-daujourdhui-gt-plus-dip/msg579684/#msg579684 )
        
    Or use online generator script from kgersen: https://jsfiddle.net/kgersen/3mnsc6wy/embedded/result/
        
        
 - Line 160 : DHCP User Class Option
    - For Livebox 4 : 
        
            client-option "send user-class &quot;\053FSVDSL_livebox.Internet.softathome.Livebox4&quot;;"
    - For Livebox 3 : 
        
            client-option "send user-class &quot;+FSVDSL_livebox.Internet.softathome.Livebox3&quot;;"

 - Line 161 : DHCP client-identifier option

        client-option "send dhcp-client-identifier 1:YY:YY:YY:YY:YY:YY;"
        
    Replace `YY:YY:YY:YY:YY:YY` by your LIVEBOX MAC Address

 - Line 260 : LIVEBOX-TV MAC Adress for Static Mapping

        mac-address ZZ:ZZ:ZZ:ZZ:ZZ:ZZ

    Replace `ZZ:ZZ:ZZ:ZZ:ZZ:ZZ` by your LIVEBOX-TV MAC Address

 - Line 262 : DHCP Option 125 for LIVEBOX-TV
    More information source : 
     - https://lafibre.info/remplacer-livebox/tuto-remplacer-la-livebox-par-un-routeur-dd-wrt-internet-tv/msg583625/#msg583625
     - https://lafibre.info/remplacer-livebox/en-cours-remplacer-sa-livebox-par-un-routeur-ubiquiti-edgemax/msg584967/#msg584967

        
            static-mapping-parameters "option Vendor-specific 00:00:0d:e9:24:04:06:YY:YY:YY:YY:YY:YY:05:0f:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:06:09:4c:69:76:65:62:6f:78:20:VV;"
        
    Replace `YY:YY:YY:YY:YY:YY` With 3 first bytes of hexa encoded LIVEBOX MAC Address
		
    Replace `XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX` with LIVEBOX Serial Number encoded in hexa
        
    Replace `VV` with livebox release encoded in hexa (33 V3, 34 V4)

 - Line 278 : LIVEBOX MAX Adress for Static Mapping

        mac-address YY:YY:YY:YY:YY:YY

    Replace `YY:YY:YY:YY:YY:YY` by your LIVEBOX MAC Address

 - Line 283 : DHCP Option domain search for VOIP
    More information source :   
    - https://lafibre.info/remplacer-livebox/cacking-nouveau-systeme-de-generation-de-loption-90-dhcp/msg596704/#msg596704

            subnet-parameters "option domain-search &quot;XXX.access.orange-multimedia.net.&quot;;"
        
    Replace `XXX` with your local prefix retrieve from /run/dhclient_eth1_832.leases or with Livebox Info Tool

In etc/dibbler/client.conf : 
 - Line 20 and 21 : DHCP Option 11 Orange Authentication String
 
        option 11 hex 00:00:00:00:00:00:00:00:00:00:00:1a:09:00:00:05:58:01:03:41:01:0d:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:3c:12:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:03:13:zz:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh
        option 11 hex 00:00:00:00:00:00:00:00:00:00:00:1a:09:00:00:05:58:01:03:41:01:0d:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:3c:12:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:03:13:zz:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh

        
    Replace `XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX` by your Orange ISP Identifier "fti/xxxxxx" encoded in Hexa.

    Using Livebox Into tool (https://www.liveboxinfo.ga/) to retrieve salt and hash value for replacing value :  `3c:12:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:sa:lt:03:13:zz:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh:ha:sh` ( https://lafibre.info/remplacer-livebox/renew-dhcp-daujourdhui-gt-plus-dip/msg579684/#msg579684 )
        
    Or use online generator script from kgersen: https://jsfiddle.net/kgersen/3mnsc6wy/embedded/result/

Requirements
------------

ERL Firmware release >= 1.10

Credits
------------
https://lafibre.info forum community